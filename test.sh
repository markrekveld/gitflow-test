#!/bin/bash -e

toolchains=''

while read -r line
do
  echo "processing $line"
  jdkHome="${line%/bin/java*}"
  if [[ -f "${jdkHome}/bin/javac" ]] && [[ "$toolchains" != *"$jdkHome"* ]] && [[ "$PATH" != *"$jdkHome"* ]]; then
    version=$($line -fullversion 2>&1 | awk -F '"' '/version/ {print $2}')
    majorVersion=$(echo "$version" | cut -d '.' -f 1)
    minorVersion=$(echo "$version" | cut -d '.' -f 2)
    if [[ "$majorVersion" == "1" ]]; then
      jdkVersion="${majorVersion}.${minorVersion}"
    else
      jdkVersion="$majorVersion"
    fi
    echo "Found JDK Version $jdkVersion in $jdkHome"
    toolchains="${toolchains}
    <toolchain>
      <type>jdk</type>
      <provides>
        <version>$jdkVersion</version>
      </provides>
      <configuration>
        <jdkHome>$jdkHome</jdkHome>
      </configuration>
    </toolchain>"
  elif [[ "$toolchains" == *"$jdkHome"* ]]; then
    echo "Ignoring duplicate JDK $jdkHome"
  else
    echo "Ignoring $line, not a JDK"
  fi
done < <(find / -name java | grep '/bin/java$')

echo "<toolchains xmlns=\"http://maven.apache.org/TOOLCHAINS/1.1.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
        xsi:schemaLocation=\"http://maven.apache.org/TOOLCHAINS/1.1.0 http://maven.apache.org/xsd/toolchains-1.1.0.xsd\">${toolchains}
</toolchains>"
